package com.tdd.kata;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LCDTest {

    private LCD sut;

    @Before
    public void setUp() throws Exception {
        sut = new LCD();
    }

    @Test
    public void shouldPrint0() throws Exception {
        //Given
        int value = 0;

        //When
        String lcdString = sut.print(value);

        //Then
        String expected =
                " _ \n" +
                "| |\n" +
                "|_|";
        assertEquals(expected, lcdString);
    }

    @Test
    public void shouldPrint3() throws Exception {
        //Given
        int value = 3;

        //When
        String lcdString = sut.print(value);

        //Then
        String expected =
                " _ \n" +
                " _|\n" +
                " _|";
        assertEquals(expected, lcdString);
    }

    @Test
    public void shouldPrint1234567890() throws Exception {
        //Given
        int value = 1234567890;

        //When
        String lcdString = sut.print(value);

        //Then
        String expected =
                "     _   _       _   _   _   _   _   _ \n" +
                "  |  _|  _| |_| |_  |_    | |_| |_| | |\n" +
                "  | |_   _|   |  _| |_|   | |_|   | |_|";
        assertEquals(expected, lcdString);
    }
}
