package com.tdd.kata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class LCD {

    public String print(int value) {
        List<Symbol> symbols = convertToSymbols(value);
        return Stream.of(
                getLine(symbols, Symbol::getTop),
                getLine(symbols, Symbol::getMiddle),
                getLine(symbols, Symbol::getBottom))
                .collect(joining("\n"));
    }

    private List<Symbol> convertToSymbols(int value){
        return convertToList(value).stream().map(Symbol::from).collect(toList());
    }

    private List<Integer> convertToList(int value){
        ArrayList<Integer> list = new ArrayList<>();
        while (value > 10){
            list.add(value % 10);
            value /= 10;
        }
        list.add(value);
        Collections.reverse(list);
        return list;
    }

    private String getLine(List<Symbol> symbols, Function<Symbol, String> function){
        return symbols.stream().map(function).collect(joining(" "));
    }

    public static void main(String[] args) {
        System.out.println(new LCD().print(192));
        System.out.println(new LCD().print(348));
        System.out.println(new LCD().print(987008765));
    }
}
