package com.tdd.kata;

import java.util.Arrays;

public enum Symbol {
    ZERO(0, " _ ",
            "| |",
            "|_|"),
    ONE(1,  "   ",
            "  |",
            "  |"),
    TWO(2,  " _ ",
            " _|",
            "|_ "),
    THREE(3," _ ",
            " _|",
            " _|"),
    FOUR(4, "   ",
            "|_|",
            "  |"),
    FIVE(5, " _ ",
            "|_ ",
            " _|"),
    SIX(6,  " _ ",
            "|_ ",
            "|_|"),
    SEVEN(7," _ ",
            "  |",
            "  |"),
    EIGHT(8," _ ",
            "|_|",
            "|_|"),
    NINE(9, " _ ",
            "|_|",
            "  |");

    private int value;
    private String top;
    private String middle;
    private String bottom;

    Symbol(int value, String top, String middle, String bottom) {
        this.value = value;
        this.top = top;
        this.middle = middle;
        this.bottom = bottom;
    }

    public String getTop() {
        return top;
    }

    public String getMiddle() {
        return middle;
    }

    public String getBottom() {
        return bottom;
    }

    public static Symbol from(final int value){
        return Arrays.stream(Symbol.values()).filter(symbol -> symbol.value == value).findAny().get();
    }
}
